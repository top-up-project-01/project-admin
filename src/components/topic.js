import {
    List, Datagrid, Edit, Create, SimpleForm,
    TextField, DateField, DateInput, TextInput, NumberInput, AutocompleteInput, ReferenceInput
} from 'react-admin';

export const TopicList = (props) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="name" label="Name" />
            <TextField source="topicCode" label="Topic's Code" />
            <TextField source="description" label="Description" />
            <DateField label="First deadline (MM/DD/YYYY)" source="firstDeadline" />
            <DateField label="Second deadline (MM/DD/YYYY)" source="secondDeadline" />
            <DateField label="Publish Date (MM/DD/YYYY)" source="publishDate" />
        </Datagrid>
    </List>
);

const TopicTitle = ({ record }) => {
    return <span>Topic {record ? `"${record.id}"` : ''}</span>;
};

const ElementInputs = () => {
    return (
        <div>
            <div>
                <TextInput source="description" label="Description" />
            </div>

            <DateInput source="firstDeadline" label="First Deadline"/>

            <div>
                <DateInput source="secondDeadline" label="Second Deadline"/>
            </div>

            <div>
                <TextInput source="topicCode" label="Topic's Code" />
            </div>

            <div>
                <TextInput source="name" label="Topic's Name" />
            </div>
        </div>
    );
};

export const TopicCreate = (props) => (
    <Create title="Create a Topic" {...props}>
        <SimpleForm>
            <ElementInputs />
        </SimpleForm>
    </Create>
);

export const TopicEdit = (props) => (
    <Edit title={<TopicTitle />} {...props}>
        <SimpleForm>
            <TextInput disabled source="id" label="ID" />
            <ElementInputs />
            <DateField label="Register time (MM/DD/YYYY)" source="createdAt" showTime />
        </SimpleForm>
    </Edit>
);

