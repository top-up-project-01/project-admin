import React from 'react';
import {
    List, Datagrid, Edit, Create, SimpleForm,
    TextField, DateField,
    ReferenceArrayInput, AutocompleteArrayInput, TextInput,
} from 'react-admin';
import UserGroup from '@material-ui/icons/SupervisorAccount';
export const FacultyIcon = UserGroup;

export const FacultyList = (props) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <TextField source="name" label="Name" />
            <TextField source="description" label="Description" />
        </Datagrid>
    </List>
);

const ElementInputs = () => {
    return (
        <div>            
            <div>
                <TextInput source="name" label="Name" />
            </div>

            <div>
                <TextInput source="description" label="Description" />
            </div>
        </div>
    );
};

const FacultyTitle = ({ record }) => {
    return <span>Faculty {record ? `"${record.id}"` : ''}</span>;
};

export const FacultyEdit = (props) => (
    <Edit title={<FacultyTitle />} {...props}>
        <SimpleForm>
            <TextInput disabled source="id" label="ID" />
            <ElementInputs />
        </SimpleForm>
    </Edit>
);

export const FacultyCreate = (props) => (
    <Create title="Create a Faculty" {...props}>
        <SimpleForm>
            <ElementInputs />
        </SimpleForm>
    </Create>
);
