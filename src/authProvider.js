import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK } from 'react-admin';
import client from './boot/graphqlClient';
import gql from "graphql-tag";

export default (type, params) => {
    if (type === AUTH_LOGIN) {
        const { username, password } = params;

        client.mutate({
            mutation: gql`
              mutation loginAdmin($user: UserPayloadInput) {
                loginAdmin(user: $user) {
                  token
                }
              }
            `,
            variables: {
                user: {
                    email: username,
                    password: password
                }
            }
        }).then(response => {
            const token = response.data.loginAdmin.token;
            localStorage.setItem("token", token);
            window.location.hash = '/';
        }).catch(() => {
            Promise.reject('Unauthorized. You cannot login.');
        });
    }

    if (type === AUTH_LOGOUT) {
        localStorage.removeItem('token');
        return Promise.resolve();
    }

    if (type === AUTH_ERROR) {
        const status  = params.status;
        if (status === 401 || status === 403) {
            localStorage.removeItem('token');
            return Promise.reject();
        }
        return Promise.resolve();
    }

    if (type === AUTH_CHECK) {
        return localStorage.getItem('token') ? Promise.resolve() : Promise.reject({ redirectTo: '/login' });
    }

    return Promise.reject('Unknown method');
}
