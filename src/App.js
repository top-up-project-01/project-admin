import React, { Component } from 'react';
import client from './boot/graphqlClient';
import authProvider from './authProvider';

import buildGraphQLProvider from 'ra-data-graphql-simple';

import { UserIcon, UserList, UserCreate, UserEdit } from "./components/user";
import { FacultyCreate, FacultyEdit, FacultyList, FacultyIcon } from './components/faculty'
import { TopicCreate, TopicList, TopicEdit } from './components/topic'

import {
  Admin, Resource,
} from 'react-admin';

class App extends Component {
  constructor() {
    super();
    this.state = { dataProvider: null };
  }
  componentDidMount() {
    buildGraphQLProvider({
      client: client
    })
      .then(dataProvider => this.setState({ dataProvider }));
  }

  render() {
    const { dataProvider } = this.state;

    if (!dataProvider) {
      return <div>Loading</div>;
    }

    return (
      <Admin authProvider={authProvider} dataProvider={dataProvider}>
        <Resource name="User" list={UserList} edit={UserEdit} create={UserCreate} icon={UserIcon} />
        <Resource name="Faculty" list={FacultyList} edit={FacultyEdit} create={FacultyCreate} icon={FacultyIcon} />
        <Resource name="Topic" list={TopicList} edit={TopicEdit} create={TopicCreate} icon={FacultyIcon} />
      </Admin>
    );
  }
}

export default App;

